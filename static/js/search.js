function displayResults (results, store) {
  const searchResults = document.getElementById('results')

  if (results.length) {
    let resultList = `<div style="margin-bottom: 30px;text-align: right;">Found <span id="post-count"></span> articles</div><div class="posts">`
    // Iterate and build result list elements
    for (const n in results) {
      const item = store[results[n].ref]
      resultList += `
      <div class="post">
        <div class="post-header">
          <div class="meta">
            <div class="date">
              <span class="day">${item.day}</span>
              <span class="rest">${item.rest}</span>
            </div>
          </div>
          <div class="matter">
            <h3 class="title small"><a href="${item.url}">${item.title}</a></h3>
            <span class="description">
              ${item.summary}
            </span>
          </div>
        </div>
      </div>
      `
    }
    resultList += '</div>'
    searchResults.innerHTML = resultList

    postcount = document.querySelectorAll('.posts .post').length
    document.querySelector("#post-count").textContent = postcount
  } else {
    searchResults.innerHTML = 'Sorry, we don\'t have any related articles.'
  }
}

// Get the query parameter(s)
const params = new URLSearchParams(window.location.search)
const query = params.get('q')

// Perform a search if there is a query
if (query) {
  // Retain the search input in the form when displaying results
  document.getElementById('q').setAttribute('value', query)

  const idx = lunr(function () {
    this.ref('id')
    this.field('author')
    this.field('summary', {
      boost: 20
    })
    this.field('title', {
      boost: 15
    })
    this.field('content', {
      boost: 10
    })

    for (const key in window.store) {
      this.add({
        id: key,
        title: window.store[key].title,
        summary: window.store[key].summary,
        author: window.store[key].author,
        tags: window.store[key].category,
        content: window.store[key].content
      })
    }
  })

  // Perform the search
  const results = idx.search(query)
  // Update the list with results
  displayResults(results, window.store)
}
